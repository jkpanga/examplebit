package com.boomi.connector.versionexample;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;

public class VersionExampleConnector extends BaseConnector {

    @Override
    public Browser createBrowser(BrowseContext context) {
        return new VersionExampleBrowser(createConnection(context));
    }    

    @Override
    protected Operation createGetOperation(OperationContext context) {
        return new VersionExampleGetOperation(createConnection(context));
    }
   
    private VersionExampleConnection createConnection(BrowseContext context) {
        return new VersionExampleConnection(context);
        //another change
        //another change
    }
}