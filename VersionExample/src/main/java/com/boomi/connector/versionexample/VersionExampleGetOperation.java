package com.boomi.connector.versionexample;

import com.boomi.connector.api.GetRequest;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.util.BaseGetOperation;

public class VersionExampleGetOperation extends BaseGetOperation {

    protected VersionExampleGetOperation(VersionExampleConnection conn) {
        super(conn);
    }

	@Override
	protected void executeGet(GetRequest request, OperationResponse response) {
		// TODO Auto-generated method stub
		//this is a sample change
		
	}

	@Override
    public VersionExampleConnection getConnection() {
        return (VersionExampleConnection) super.getConnection();
    }
}