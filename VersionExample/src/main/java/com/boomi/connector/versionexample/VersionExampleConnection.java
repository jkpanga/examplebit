package com.boomi.connector.versionexample;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.util.BaseConnection;

public class VersionExampleConnection extends BaseConnection {

	public VersionExampleConnection(BrowseContext context) {
		super(context);
	}
	
}